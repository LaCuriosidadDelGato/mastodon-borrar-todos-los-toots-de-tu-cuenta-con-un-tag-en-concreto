#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
# Descargar libreria Mastodon en: https://github.com/halcy/Mastodon.py
from mastodon import Mastodon
 
API_BASE_URL = 'https://qoto.org'       # url de la instancia
CUENTA_CORREO = 'correo@algo.com'       # correo usado para crear la cuenta
CUENTA_PASS = 'contraseña'              # contraseña de la cuenta
TAG_BORRAR = 'tag_a_borrar'             # el tag que tiene que tener el toot para ser borrado sin la #
 
def existe_tag(tag, estado):
    for t in estado['tags']:
        if t['name']==tag:
            return True
    return False
   
Mastodon.create_app(
     'pytooterapp',
     api_base_url = API_BASE_URL,
     to_file = 'pytooter_clientcred.secret'
)
 
mastodon = Mastodon(
    client_id = 'pytooter_clientcred.secret',
    api_base_url = API_BASE_URL
)
 
mastodon.log_in(
    CUENTA_CORREO,
    CUENTA_PASS,
    to_file = 'pytooter_usercred.secret'
)
 
res = mastodon.toot('toot para conocer tu id') # publica un toot
id_usu = res.account.id # sacamos tu id de usuario
id_estado = 1
while True:
    toots = mastodon.account_statuses(id_usu, since_id=id_estado) # sacamos tus ultimos 20 toots
    if len(toots)<=0:
        break
       
    for toot in toots:
        res = toot.uri.split('/')
        if res[-1]=='activity':
            id_estado = res[-2]
        else:
            id_estado = res[-1]
        if existe_tag(TAG_BORRAR,toot):
            print id_estado,
            mastodon.status_delete(id_estado) # borramos
            print ' borrado'
        else:
            print id_estado
       
print 'Ya no hay mas'